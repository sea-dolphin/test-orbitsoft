@extends('default')

@section('content')

    <div class="container-fluid">
        <div class="row">
            
            @include('left_menu');
            
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                <h2 class="sub-header">Добавление книги</h2>
                <div class="table-responsive">
                    
                    <form action="/books/{{$oData->id}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        {{ method_field('PUT') }}
                        
                        <div class="input-group">
                            <span class="input-group-addon">Название</span>
                            <input name="name" type="text" class="form-control" placeholder="" required="" maxlength="150" value="{{$oData->name}}">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Автор</span>
                            <input name="author" type="text" class="form-control" placeholder="" required=""  maxlength="100" value="{{$oData->author}}">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Год издания</span>
                            <input name="year_of_publishing" type="text" class="form-control" placeholder="" value="{{$oData->year_of_publishing}}">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="comment">Описание:</label>
                            <textarea name="description" class="form-control" rows="5" id="comment" required="" maxlength="2000">{{$oData->description}}</textarea>
                        </div>
                        
                        <span class="btn btn-default btn-file">
                            Выбрать изображение <input type="file" name="file">
                        </span>
                        <br>
                        <p>
                        <strong>Обложка:</strong><br>
                            @if (!empty($oData->image))
                                <img src="/download_user/{{$oData->image}}" width="400">
                            @endif
                        </p>
                        <br>
                        <button type="submit" class="btn btn-success">Сохранить изменения</button>
                    </form>
                    
                    <br>
                    <form action="/books/{{$oData->id}}" method="post">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        {{ method_field('DELETE') }}
                        @if (!empty($oData->image))
                            <input type="hidden" name="image_for_delete" value="{{$oData->image}}" >
                        @endif
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </form>
                    
                    @if (count($errors) > 0)
                        <br>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>

@stop