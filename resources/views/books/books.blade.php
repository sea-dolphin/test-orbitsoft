@extends('default')

@section('content')

<div class="container-fluid">
        <div class="row">
        
          @include('left_menu');
          
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<!--          <h1 class="page-header">Dashboard</h1>-->

            <h2 class="sub-header">Список книг</h2>
            <div class="table-responsive">
              
                <a href="/books/create"><div class="btn btn-success">Добавить книгу</div></a>
                <br>
                <h2 class="sub-header">Сортировка</h2>
                <form action="/books" method="get">
                    <div class="form-group">
                        <label for="usr">Строка поиска:</label>
                        <input type="text" name="search" class="form-control" id="usr" @if (!empty($aPaginationParametr['search'])) value="{{$aPaginationParametr['search']}}" @endif>
                        <label for="sel1">Сортировать по:</label>
                        <select class="form-control" id="sel1" name="filter_field">
                            <option value="name">Названию</option>
                            <option value="author" @if (!empty($aPaginationParametr['filter_field']) && $aPaginationParametr['filter_field'] == 'author') selected="" @endif>Автору</option>
                        </select>
                        <br>
                        <label class="radio-inline"><input type="radio" name="order" value="asc" @if (!empty($aPaginationParametr['order']) && $aPaginationParametr['order'] == 'asc') checked="" @endif>По возрастанию</label>
                        <label class="radio-inline"><input type="radio" name="order" value="desc" @if (!empty($aPaginationParametr['order']) && $aPaginationParametr['order'] == 'desc') checked="" @endif>По убыванию</label>
                        <br><br>
                        <button type="submit" class="btn btn-primary">Искать</button>
                    </div>
                </form>
              
              
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Действие</th>
                            <th>Название</th>
                            <th>Автор</th>
                            <th>Год издания</th>
                            <th>Описание</th>
                            <th>Обложка</th>
                        </tr>
                    </thead>
                  <tbody>
                      @foreach ($aData as $oItem)
                        <tr>
                            <td>
                                <a href="/books/{{$oItem->id}}/edit" title="редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
<!--                                <a href="/books/{{$oItem->id}}" title="удалить"><span class="glyphicon glyphicon-remove"></span></a>-->
                            </td>
                            <td><a href="/books/{{$oItem->id}}">{{$oItem->name}}</a></td>
                            <td>{{$oItem->author}}</td>
                            <td>{{$oItem->year_of_publishing}}</td>
                            <td>{{$oItem->description}}</td>
                            <td>
                                @if (!empty($oItem->image))
                                    <img src="/download_user/{{$oItem->image}}" width="50">
                                @endif
                            </td>
                        </tr>
                      @endforeach

                  </tbody>
                </table>
              
              {!!$aData->appends($aPaginationParametr)->render()!!}
              
          </div>
        </div>
      </div>
    </div>

@stop