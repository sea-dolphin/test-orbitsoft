@extends('default')

@section('content')

    <div class="container-fluid">
        <div class="row">
            
            @include('left_menu');
            
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                <h2 class="sub-header">Детальная информация о книге</h2>
                <div class="table-responsive">
                    
                    <p>
                        <strong>Название:</strong><br>
                        {{$oData->name}}
                    </p>
                    <p>
                        <strong>Автор:</strong><br>
                        {{$oData->author}}
                    </p>
                    <p>
                        <strong>Год издания:</strong><br>
                        {{$oData->year_of_publishing}}
                    </p>
                    <p>
                        <strong>Описание:</strong><br>
                        {{$oData->description}}
                    </p>
                    <p>
                        <strong>Обложка:</strong><br>
                        @if (!empty($oData->image))
                            <img src="/download_user/{{$oData->image}}" width="400">
                        @endif
                    </p>
                    
                </div>
            </div>
        </div>
    </div>

@stop