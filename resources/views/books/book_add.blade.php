@extends('default')

@section('content')

    <div class="container-fluid">
        <div class="row">
            
            @include('left_menu');
            
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                <h2 class="sub-header">Добавление книги</h2>
                <div class="table-responsive">
                    
                    <form action="/books" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        
                        <div class="input-group">
                            <span class="input-group-addon">Название</span>
                            <input name="name" type="text" class="form-control" placeholder="" required="" maxlength="150">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Автор</span>
                            <input name="author" type="text" class="form-control" placeholder="" required=""  maxlength="100">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">Год издания</span>
                            <input name="year_of_publishing" type="text" class="form-control" placeholder="">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="comment">Описание:</label>
                            <textarea name="description" class="form-control" rows="5" id="comment" required="" maxlength="2000"></textarea>
                        </div>
                        
                        <span class="btn btn-default btn-file">
                            Выбрать изображение <input type="file" name="file">
                        </span>
                        <br>
                        <br>
                        <button type="submit" class="btn btn-success">Добавить книгу</button>
                    </form>
                    
                    @if (count($errors) > 0)
                        <br>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>

@stop