<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\ImagesController;

use App\Models\Book;

class BooksController extends Controller
{
    private $pageCount = 3;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //формирую строку поиска
        $strQuery = ' 1 = ? ';
        $aQueryValues[] = 1;
        
        //упорядочивание по умолчанию
        $strField = 'id';
        $strOrder = 'DESC';
        
        if (!empty($request->input('search')))
        {
            $strQuery .= ' AND (name LIKE ? OR author LIKE ?) ';
            $aQueryValues[] = '%'.$request->input('search').'%';
            $aQueryValues[] = '%'.$request->input('search').'%';
        }
        
        if (!empty($request->input('filter_field')) && !empty($request->input('order')))
        {
            $strField = $request->input('filter_field');
            $strOrder = $request->input('order');
        }
        
        //формирование строки для пагинации
        $aPaginationParametr = [
            'search' => empty($request->input('search')) ? '' : $request->input('search'),
            'filter_field' => empty($request->input('filter_field')) ? '' : $request->input('filter_field'), 
            'order' => empty($request->input('order')) ? '' : $request->input('order'),
        ];
        
        $aData = Book::whereRaw($strQuery, $aQueryValues)
                ->orderBy($strField, $strOrder)
                ->paginate($this->pageCount);
        
        return view('books.books', ['aData' => $aData, 'aPaginationParametr' => $aPaginationParametr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.book_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //в реальном проекте валидацию нужно вынести в отдельный метод
        
        $this->validate($request, [
            'name' => 'required|max:150',
            'author' => 'required|max:100',
            'year_of_publishing' => 'numeric',
            'description' => 'required|max:2000',
        ]);
        
        $oImagesController = new ImagesController();
        $strNameImage = $oImagesController->loadImages($request);
        
        $aData = [
            'name' => $request->input('name'),
            'author' => $request->input('author'),
            'year_of_publishing' => $request->input('year_of_publishing'),
            'description' => $request->input('description'),
            'image' => $strNameImage,
        ];
        
        Book::create($aData);
        
        return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Book $oBookModel, $id)
    {
        $oData = $oBookModel->getDetailItem($id);
        return view('books.book_detail', ['oData' => $oData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $oBookModel, $id)
    {
        $oData = $oBookModel->getDetailItem($id);
        return view('books.book_edit', ['oData' => $oData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Book $oBookModel)
    {
        //в реальном проекте валидацию нужно вынести в отдельный метод
        
        $this->validate($request, [
            'name' => 'required|max:150',
            'author' => 'required|max:100',
            'year_of_publishing' => 'numeric',
            'description' => 'required|max:2000',
        ]);
        
        $oData = $oBookModel->getDetailItem($id);
        
        $oImagesController = new ImagesController();
        $strNameImage = $oImagesController->loadImages($request);
        
        //в случае выборна новой обложки, старую удаляю
        if (!empty($strNameImage))
        {
            $oImagesController->deleteImage($oData->image);
        }
        
        $aData = [
            'name' => $request->input('name'),
            'author' => $request->input('author'),
            'year_of_publishing' => $request->input('year_of_publishing'),
            'description' => $request->input('description'),
            'image' => empty($strNameImage) ? $oData->image : $strNameImage,
        ];
        
        Book::where('id', '=', $id)->update($aData);
        
        return redirect('/books/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Book::where('id', '=', $id)->delete();
        //Что бы не делать лишний запрос к базе, получаю имя картинки для удаления из скрытого поля формы
        if (!empty($request->input('image_for_delete')))
        {
            //здесь можно воспользоваться $oImagesController->deleteImage()
            @unlink('download_user/'.$request->input('image_for_delete'));
        }
        return redirect('/books/');
    }
    
    
}
